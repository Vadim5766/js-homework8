const paragraph = document.querySelectorAll("p");
for (const iterator of paragraph) {
  iterator.style.background = "#ff0000";
}
const options = document.getElementById("optionsList");
const parent = options.parentElement;
console.log(options);
console.log(parent);
for (const iterator of parent.children) {
  console.log(iterator + " " + iterator.nodeType);
}

let paragraphText = document.getElementById("testParagraph");
paragraphText = paragraphText.innerText = "This is a paragraph";

let main = document.querySelector(".main-header");
main = main.children;
console.log(main);
for (const iterator of main) {
  iterator.classList.add("nav-item");
}

let elem = document.querySelectorAll(".section-title");
for (const iterator of elem) {
  iterator.classList.remove("section-title");
}
